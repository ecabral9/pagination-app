export const TableHeaders = [
    {
        key: 'name',
        title: 'Name'
    },
    {
        key: 'phone',
        title: 'Phone'
    },
    {
        key: 'email',
        title: 'Email'
    },
    {
        key: 'company',
        title: 'Company'
    },
    {
        key: 'address_1',
        title: 'Address'
    },
    {
        key: 'city',
        title: 'City'
    },
    {
        key: 'status',
        title: 'Status'
    },
    {
        key: 'fee',
        title: 'Fee'
    },
    {
        key: 'date_recent',
        title: 'Recent Date',
        dateFormat: true
    }
];
