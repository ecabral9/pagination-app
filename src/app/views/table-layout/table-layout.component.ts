import {Component, OnInit} from '@angular/core';
import {TableHeaders} from '../../constants/table-headers';
import {HttpService} from 'src/app/services/http-services';

@Component({
    selector: 'app-table-layout',
    templateUrl: './table-layout.component.html',
    styleUrls: ['./table-layout.component.scss']
})
export class TableLayoutComponent implements OnInit {

    tableHeaders: any[];
    tableData: any[];
    tableActions: any[];
    constructor(private httpService: HttpService) {}

    ngOnInit() {
        this.tableHeaders = TableHeaders;
        this.fetchData();
        this.tableActions = [{icon: 'fa-external-link link'}];
    }

    fetchData(): void {
        this.httpService.getTableData().subscribe((data) => {
            this.tableData = data;
        }, error => {
            console.log('An error has occured' + error);
        });
    }

    actionRow(event: any): void {
        this.httpService.submitRowData(event.event).subscribe(data => {
            console.log('Success');
        }, error => console.log('An error has occured' + error.error));
    }

}
