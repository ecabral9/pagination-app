import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TableLayoutComponent} from './table-layout.component';
import {TableComponent} from 'src/app/components';
import {HttpService} from 'src/app/services';
import {HttpClientModule} from '@angular/common/http';

describe('TableLayoutComponent', () => {
    let component: TableLayoutComponent;
    let fixture: ComponentFixture<TableLayoutComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TableLayoutComponent, TableComponent],
            imports: [HttpClientModule],
            providers: [HttpService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TableLayoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
