import {Component, OnInit, HostListener} from '@angular/core';
import {TableHeaders} from '../../constants/table-headers';
import {HttpService} from 'src/app/services/http-services';
import {cloneDeep} from 'lodash';

@Component({
    selector: 'app-table-layout-pagination',
    templateUrl: './table-layout-pagination.component.html',
    styleUrls: ['./table-layout-pagination.component.scss']
})
export class TableLayoutPaginationComponent implements OnInit {

    tableHeaders: any[];
    tableData: any[];
    paginatedRecords: any[];
    totalTableRecords: number;
    selectableDropdownValues: number[];
    tableActions: any[];
    constructor(private httpService: HttpService) {}


    @HostListener('window:scroll', [])
    scrollToBottom(): void {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            const recordsToAdd = cloneDeep(this.paginatedRecords);
            if (this.paginatedRecords.length) {
                recordsToAdd.forEach(record => {
                    this.paginatedRecords.push(record);
                });
            }
        }
    }

    ngOnInit() {
        this.tableHeaders = TableHeaders;
        this.fetchDataFromJson();
        this.selectableDropdownValues = [10, 25, 50, 100];
        this.tableActions = [{icon: 'fa-external-link link'}];
    }

    fetchDataFromJson(): void {
        this.httpService.getTableData().subscribe((data) => {
            this.configureTableData(data);
        }, error => {
            console.log('An error has occured' + error);
        });
    }

    fetchRecords(event: Event): void {
        this.paginatedRecords = this.tableData.filter(record => record.id >= 1 && record.id <= event);
    }

    configureTableData(data: any[]): void {
        this.tableData = data;
        this.totalTableRecords = this.tableData.length;
        this.selectableDropdownValues.push(this.totalTableRecords);
        this.paginatedRecords = cloneDeep(this.tableData);
        // add index to distinguish the record
        this.paginatedRecords.forEach((record, index = 1) => {
            record.index = index;
        });
        this.paginatedRecords = this.paginatedRecords.filter(record => record.index >= 1
            && record.index <= this.selectableDropdownValues[0]);
    }

    actionRow(event: any): void {
        this.httpService.submitRowData(event.event).subscribe(data => {
            console.log('Success');
        }, error => console.log('An error has occured' + error.error));
    }

}
