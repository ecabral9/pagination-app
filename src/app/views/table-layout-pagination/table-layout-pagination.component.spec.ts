import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TableLayoutPaginationComponent} from './table-layout-pagination.component';

import {HttpService} from '../../services';
import {TableComponent, PaginationComponent} from 'src/app/components';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

describe('TableLayoutPaginationComponent', () => {
    let component: TableLayoutPaginationComponent;
    let fixture: ComponentFixture<TableLayoutPaginationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TableLayoutPaginationComponent, TableComponent, PaginationComponent],
            imports: [HttpClientModule, FormsModule],
            providers: [HttpService, HttpClient]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TableLayoutPaginationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
