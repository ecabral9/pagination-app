import {BaseLayoutComponent} from './base-layout/base-layout.component';
export {BaseLayoutComponent} from './base-layout/base-layout.component';

import {TableLayoutComponent} from './table-layout/table-layout.component';
export {TableLayoutComponent} from './table-layout/table-layout.component';

import {TableLayoutPaginationComponent} from './table-layout-pagination/table-layout-pagination.component';
export {TableLayoutPaginationComponent} from './table-layout-pagination/table-layout-pagination.component';

export const views: any[] = [
    BaseLayoutComponent,
    TableLayoutComponent,
    TableLayoutPaginationComponent
];
