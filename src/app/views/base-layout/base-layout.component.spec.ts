import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BaseLayoutComponent} from './base-layout.component';
import {HeaderComponent} from 'src/app/components';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

describe('BaseLayoutComponent', () => {
    let component: BaseLayoutComponent;
    let fixture: ComponentFixture<BaseLayoutComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BaseLayoutComponent, HeaderComponent],
            imports: [FormsModule, RouterTestingModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BaseLayoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
