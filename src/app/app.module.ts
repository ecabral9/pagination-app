import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {components} from './components/index';
import {views} from './views/index';
import {HttpClientModule} from '@angular/common/http';
import {HttpService} from './services/http-services';
import {FormsModule} from '@angular/forms';


@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    declarations: [
        AppComponent,
        ...components,
        ...views
    ],
    entryComponents: [
        ...components,
        ...views
    ],
    providers: [
        HttpService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
