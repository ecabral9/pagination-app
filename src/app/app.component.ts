import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: '<app-base-layout [title]="title"></app-base-layout>'
})
export class AppComponent {
    title = 'Pagination App';
}
