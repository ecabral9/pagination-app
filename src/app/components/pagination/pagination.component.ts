import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

    @Input() selectableDropdownValues: number[];
    pageSize = 10;
    @Input() totalRecords: number;
    @Output() rowsToDisplayEmitter: EventEmitter<number> = new EventEmitter<number>();
    constructor() {}

    ngOnInit() {
    }

    fetchRecordByRecordCount(): void {
        this.rowsToDisplayEmitter.emit(this.pageSize);
    }

}
