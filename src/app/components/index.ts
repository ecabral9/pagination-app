import {HeaderComponent} from './header/header.component';
export {HeaderComponent} from './header/header.component';

import {PaginationComponent} from './pagination/pagination.component';
export {PaginationComponent} from './pagination/pagination.component';
import {TableComponent} from './table/table.component';
export {TableComponent} from './table/table.component';

export const components: any[] = [
    HeaderComponent,
    TableComponent,
    PaginationComponent
];
