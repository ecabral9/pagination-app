import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    @Input() headers: any[];
    @Input() data: any[];
    @Input() tableActions: any[];
    @Output() selectedRowEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor() {}

    ngOnInit() {
    }

    selectRow(row: any): void {
        this.selectedRowEmitter.emit({event: row});
    }

}
