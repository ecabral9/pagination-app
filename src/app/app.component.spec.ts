import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {components} from './components/index';
import {views} from 'src/app/views';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {services} from 'src/app/services';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                FormsModule,
                HttpClientModule
            ],
            declarations: [
                AppComponent,
                components,
                views
            ],
            providers: [
                services
            ]
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'Pagination App'`, () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('Pagination App');
    });
});
