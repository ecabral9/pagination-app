import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TableLayoutComponent, TableLayoutPaginationComponent} from './views/index';

const routes: Routes = [
    {
        path: '',
        component: TableLayoutPaginationComponent
    },
    {
        path: 'no-pagination',
        component: TableLayoutComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
