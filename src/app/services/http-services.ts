import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {from} from 'rxjs/observable/from';

@Injectable({
    providedIn: 'root',
})
export class HttpService {

    constructor(private http: HttpClient) {
    }

    getTableData(): Observable<any> {
        return from(this.http.get('../../assets/data/sample_data.json'));
    }

    submitRowData(row: any): Observable<any> {
        return from(this.http.post('/api/submit', {id: row.id, status: row.status}));
    }
}
